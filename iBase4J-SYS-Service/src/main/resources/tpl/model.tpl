package org.ibase4j.model.{MODULE};

import org.ibase4j.core.base.BaseModel;

/**
 * {POJONAME}实体类
 * @author ShenHuaJie
 * @version {CURR_TIME}
 */
@SuppressWarnings("serial")
public class {POJONAME} extends BaseModel {
	{{GET_SET}}
}